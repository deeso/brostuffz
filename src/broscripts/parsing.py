# Copyright 2013 Adam Pridgen

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.



import time
import datetime


BRO_TYPE_MAPPING = {
    'time':lambda value: 0.0 if not value.replace('.','').isdigit() else time.ctime(float(value)),
    'string': lambda value: unicode(value),
    'count': lambda value: -1 if not value.isdigit() else int(value),
    'counter': lambda value: -1 if not value.isdigit() else int(value),
    'bool': lambda value: False if value == 'F' else True,
    'addr': lambda value: unicode(value),
    'subnet': lambda value: unicode(value),
    'interval': lambda value: 0.0 if not value.replace('.','').isdigit() else float(value),
    'port': lambda value: 0 if not value.isdigit() else int(value),
    # Not sure how to handle table[string] yet, so it will just be a string for the time being
    'table': lambda value: unicode(value),
    # Not sure how to handle enums, but since they are fix values, we just make them strings
    'enum':lambda value: unicode(value),
    # Bro function not interpreting it
    'func':lambda value: unicode(value),
    'func':lambda value: unicode(value),
    'double': lambda value: 0.0 if not value.replace('.','').isdigit() else float(value),
    'event': lambda value: unicode(value),
    'file': lambda value: unicode(value),
    'record': lambda value: unicode(value),
    'pattern': lambda value: unicode(value)
}



class BrologEntry(object):
    def __init__(self, logtype, **kargs):
        self.type = logtype
        for k, v in kargs.items():
            setattr(self, k.replace('.','_'), v)


class BrologParserBase(object):

    def __init__(self, logname, logtype=None ):
        
        self.data = [i.strip() for i in open(logname).read().splitlines()]
        
        setattr(self, 'logname', logname)
        setattr(self, 'entrys', [])
        
        setattr(self, 'fields', [])
        setattr(self, 'types', [])
        setattr(self, 'field_types', {})
        
        setattr(self, 'close', datetime.datetime.now())
        setattr(self, 'open', datetime.datetime.now())
        
        setattr(self, 'path', datetime.datetime.now())
        setattr(self, 'unset_field', '-')
        setattr(self, 'empty_field', '(empty)')
        setattr(self, 'set_separator', ',')
        setattr(self, 'separator', '\x09')
        setattr(self, 'metas_set', set())

        fields_found = False
        types_found = False
        
        cnt = 0
        self.data_line_offset = 0
        
        while self.data[cnt].find('#') == 0:

            meta = self.handle_meta_tag(self.data[cnt])
            self.metas_set.add(meta)

            cnt += 1

        self.data_line_offset = cnt
        if not 'types' in self.metas_set and \
            not 'fields' in self.metas_set:
            raise Exception("Fields and types could not be determined at parse time")

        
        self.set_field_types()
            
            

    def set_field_types(self):
        if len(self.fields) != len(self.types):
            raise Exception("The length of the fields and types array are not equal")

        pos = 0
        while pos < len(self.types):
            self.field_types[self.fields[pos]] = self.types[pos]
            pos += 1

    def handle_meta_tag(self, data):
        if data.find('#separator') == 0:
            self.separator = data.split()[1].decode('unicode_escape')
            return 'separator'
        
        elif data.find('#set_separator') == 0:
            self.set_separator = self.brolog_split(data)[1].decode('unicode_escape')
            return 'set_separator'

        elif data.find('#empty_field') == 0:
            self.empty_field = self.brolog_split(data)[1].decode('unicode_escape')
            return 'empty_field'

        elif data.find('#unset_field') == 0:
            self.unset_field = self.brolog_split(data)[1].decode('unicode_escape')
            return 'unset_field'

        elif data.find('#path') == 0:
            self.path = self.brolog_split(data)[1].decode('unicode_escape')
            return 'path'

        elif data.find('#open') == 0:
            date = self.brolog_split(data)[1].decode('unicode_escape')
            date_items = [int(i.strip()) for i in date.split('-')]
            self.open = datetime.datetime(*date_items)
            return 'open'

        elif data.find('#close') == 0:
            date = self.brolog_split(data)[1].decode('unicode_escape')
            date_items = [int(i.strip()) for i in date.split('-')]
            self.close = datetime.datetime(*date_items)
            return 'close'
        
        elif data.find('#fields') == 0:
            self.fields = self.brolog_split(data)[1:]
            return 'fields'

        elif data.find('#types') == 0:
            self.types = self.brolog_split(data)[1:]
            return 'types'

        raise Exception("Unknown meta tag: %s"%data)

    def brolog_split(self, value):
        return value.split(self.separator)


    def parse_lines(self):
        cnt = self.data_line_offset
        while cnt < len(self.data):
            if self.data[cnt][0] == '#':
                tag_line = self.brolog_split(self.data[cnt])
                if tag_line[0].find('#close') == 0:
                    self.handle_meta_tag(self.data[cnt])
                    break
                raise Exception("Unexpected metatag encountered: %s"%tag_line)
            
            data_fields = self.brolog_split(self.data[cnt])
            
            if len(data_fields) != len(self.fields):
                raise Exception("Data fields length did not match the expected field length: %d"%(len(self.fields)))
            
            pos = 0
            kargs = {}                
            while pos < len(data_fields):
                field_name = self.fields[pos]
                v = data_fields[pos]
                value = self.get_value(v, self.field_types[field_name])
                kargs[field_name] = value
                pos += 1
            
            self.entrys.append(BrologEntry(self.path, **kargs))
            cnt += 1


    def get_value(self, value, type_):
        if value == self.empty_field:
            return None
        elif value == self.unset_field:
            return None
        #elif not type_ in BRO_TYPE_MAPPING:
        #    raise Exception("Could not find %s-type in Bro Type Mappings, it needs to be added"%type_)
        
        #handle collections separately
        if type_.find('set') == 0:
            raise Exception("Not handling sets yet")
        elif type_.find('table') == 0:
            t_type = type_.split('table[')[1].split(']')[0]
            return BRO_TYPE_MAPPING[t_type](value)
        elif type_.find('vector') == 0:
            v_type = type_.split('vector[')[1].split(']')[0]
            #values = [BRO_TYPE_MAPPING[v_type](i) for i in ]
            return BRO_TYPE_MAPPING[v_type](value)
        elif type_.find('record') > -1:
            return unicode(value)

        if type_ in BRO_TYPE_MAPPING:
            return BRO_TYPE_MAPPING[type_](value)
        raise Exception("Could not find %s-type in Bro Type Mappings, it needs to be added"%type_)
